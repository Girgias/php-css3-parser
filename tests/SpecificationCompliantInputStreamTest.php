<?php

/**
 * MIT License.
 *
 * Copyright (c) 2019 George Peter Banyard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace Tests;

use Girgias\CSSParser\Exception\ParseError;
use Girgias\CSSParser\SpecificationCompliantInputStream;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class SpecificationCompliantInputStreamTest extends TestCase
{
    /**
     * @covers \Girgias\CSSParser\SpecificationCompliantInputStream::determineEncoding
     */
    public function testCharsetRuleUtf8(): void
    {
        $input = "@charset \"utf-8\";\ninput stream now starts";
        $inputStream = new SpecificationCompliantInputStream($input);
        $preprocessedStream = $this->getInputStreamCodePoints($inputStream);
        self::assertSame(
            "\ninput stream now starts",
            $preprocessedStream
        );
    }

    /**
     * @covers \Girgias\CSSParser\SpecificationCompliantInputStream::determineEncoding
     */
    public function testCharsetRuleEmpty(): void
    {
        $input = "@charset \"\";\ninput stream now starts";
        $this->expectException(ParseError::class);
        $this->expectExceptionMessage('Charset declaration for file is empty on line 1:0');
        new SpecificationCompliantInputStream($input);
    }

    /**
     * @covers \Girgias\CSSParser\SpecificationCompliantInputStream::__construct
     * @covers \Girgias\CSSParser\SpecificationCompliantInputStream::determineEncoding
     */
    public function testCharsetRuleNonUtf8(): void
    {
        $input = "@charset \"ISO-8859-1\";\ninput stream now starts";
        $inputStream = new SpecificationCompliantInputStream($input);
        $preprocessedStream = $this->getInputStreamCodePoints($inputStream);
        self::assertSame(
            "\ninput stream now starts",
            $preprocessedStream
        );
    }

    /**
     * @covers \Girgias\CSSParser\SpecificationCompliantInputStream::__construct
     */
    public function testRemoveUtf8Bom(): void
    {
        $input = "\xEF\xBB\xBFthis string has a utf-8 BOM";
        $inputStream = new SpecificationCompliantInputStream($input);
        $preprocessedStream = $this->getInputStreamCodePoints($inputStream);
        self::assertSame(
            'this string has a utf-8 BOM',
            $preprocessedStream
        );
    }

    /**
     * @covers \Girgias\CSSParser\SpecificationCompliantInputStream::determineEncoding
     */
    public function testCharsetRuleBogusUtf16(): void
    {
        $input = "@charset \"utf-16be\";\ninput stream now starts";
        $inputStream = new SpecificationCompliantInputStream($input);
        $preprocessedStream = $this->getInputStreamCodePoints($inputStream);
        self::assertSame(
            "\ninput stream now starts",
            $preprocessedStream
        );
        $input = "@charset \"utf-16le\";\ninput stream now starts";
        $inputStream = new SpecificationCompliantInputStream($input);
        $preprocessedStream = $this->getInputStreamCodePoints($inputStream);
        self::assertSame(
            "\ninput stream now starts",
            $preprocessedStream
        );
    }

    private function getInputStreamCodePoints(SpecificationCompliantInputStream $inputStream): string
    {
        $reflectionClass = new \ReflectionClass($inputStream);

        $reflectionProperty = $reflectionClass->getProperty('stream');
        $reflectionProperty->setAccessible(true);

        return \implode('', $reflectionProperty->getValue($inputStream));
    }
}
