<?php

/**
 * MIT License.
 *
 * Copyright (c) 2019 George Peter Banyard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace Tests\Tokens;

use Girgias\CSSParser\CompleteLexer;
use Girgias\CSSParser\Lexer;
use Girgias\CSSParser\SpecificationCompliantInputStream;
use Girgias\CSSParser\SpecificationCompliantLexer;
use Girgias\CSSParser\Tokens\Dimension;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class DimensionTest extends TestCase
{
    /**
     * @covers \Girgias\CSSParser\Tokens\Dimension
     * @dataProvider validDimensionsProvider
     *
     * @param float|int $rawValue
     */
    public function testValidDimensions(string $input, string $sign, $rawValue, string $unit): void
    {
        $token = $this->getLexer($input)->readNext();
        self::assertInstanceOf(Dimension::class, $token);
        self::assertSame($sign, $token->getNumberToken()->getSign());
        self::assertSame($rawValue, $token->getNumberToken()->getRawValue());
        self::assertSame($unit, $token->getUnit());
    }

    public function validDimensionsProvider(): array
    {
        return [
            ['12px', '', 12, 'px'],
            ['1rem', '', 1, 'rem'],
            ['+1.2s', '+', 1.2, 's'],
            ['-3.4deg', '-', -3.4, 'deg'],
        ];
    }

    private function getLexer(string $input): Lexer
    {
        return new SpecificationCompliantLexer(new CompleteLexer(new SpecificationCompliantInputStream($input)));
    }
}
