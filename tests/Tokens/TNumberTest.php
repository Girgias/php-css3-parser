<?php

/**
 * MIT License.
 *
 * Copyright (c) 2019 George Peter Banyard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace Tests\Tokens;

use Girgias\CSSParser\CompleteLexer;
use Girgias\CSSParser\Lexer;
use Girgias\CSSParser\SpecificationCompliantInputStream;
use Girgias\CSSParser\SpecificationCompliantLexer;
use Girgias\CSSParser\Tokens\FloatNumber;
use Girgias\CSSParser\Tokens\IntegerNumber;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class TNumberTest extends TestCase
{
    /**
     * @covers \Girgias\CSSParser\Tokens\IntegerNumber
     * @covers \Girgias\CSSParser\Tokens\TNumber
     * @dataProvider intProvider
     */
    public function testIntegerNumbers(string $input, string $value, int $rawValue, string $sign): void
    {
        $token = $this->getLexer($input)->readNext();
        self::assertInstanceOf(IntegerNumber::class, $token);
        self::assertSame($value, $token->getValue());
        self::assertSame($rawValue, $token->getRawValue());
        self::assertSame($sign, $token->getSign());
    }

    /**
     * @covers \Girgias\CSSParser\Tokens\FloatNumber
     * @covers \Girgias\CSSParser\Tokens\TNumber
     * @dataProvider floatProvider
     */
    public function testFloatNumbers(string $input, string $value, float $rawValue, string $sign): void
    {
        $token = $this->getLexer($input)->readNext();
        self::assertInstanceOf(FloatNumber::class, $token);
        self::assertSame($value, $token->getValue());
        self::assertSame($rawValue, $token->getRawValue());
        self::assertSame($sign, $token->getSign());
    }

    public function intProvider(): array
    {
        return [
            ['-4', '-4', -4, '-'],
            ['+4', '4', 4, '+'],
            ['4', '4', 4, ''],
        ];
    }

    public function floatProvider(): array
    {
        return [
            ['-123.456e+5', '-12345600', -12345600.0, '-'],
            ['+123.456e+5', '12345600', 12345600.0, '+'],
            ['123.456e+5', '12345600', 12345600.0, ''],
            ['-32.3', '-32.3', -32.3, '-'],
            ['+32.3', '32.3', 32.3, '+'],
            ['32.3', '32.3', 32.3, ''],
        ];
    }

    private function getLexer(string $input): Lexer
    {
        return new SpecificationCompliantLexer(new CompleteLexer(new SpecificationCompliantInputStream($input)));
    }
}
