<?php

/**
 * MIT License.
 *
 * Copyright (c) 2019 George Peter Banyard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace Tests\Tokens;

use Girgias\CSSParser\CompleteLexer;
use Girgias\CSSParser\Lexer;
use Girgias\CSSParser\SpecificationCompliantInputStream;
use Girgias\CSSParser\SpecificationCompliantLexer;
use Girgias\CSSParser\Tokens\Delimiter;
use Girgias\CSSParser\Tokens\Dimension;
use Girgias\CSSParser\Tokens\EOF;
use Girgias\CSSParser\Tokens\Identifier;
use Girgias\CSSParser\Tokens\IntegerNumber;
use Girgias\CSSParser\Tokens\TNumber;
use Girgias\CSSParser\Tokens\Whitespace;
use PHPUnit\Framework\TestCase;

/**
 * This syntax is used in the CSS :nth-child()/:nth-of-type() style functions.
 *
 * @see https://www.w3.org/TR/css-syntax-3/#anb-microsyntax
 *
 * @internal
 */
final class AnPlusBParsingTest extends TestCase
{
    /**
     * @dataProvider anPlusBInputProvider
     */
    public function testAnPlusBSyntax(string $input, array $expectedTokens): void
    {
        $lexer = $this->getLexer($input);

        foreach ($expectedTokens as $expectedToken) {
            $token = $lexer->readNext();
            self::assertInstanceOf($expectedToken[0], $token);

            if ($token instanceof Dimension) {
                self::assertSame($expectedToken[1], $token->getNumberToken()->getSign());
                self::assertSame($expectedToken[2], $token->getNumberToken()->getRawValue());
                self::assertSame($expectedToken[3], $token->getUnit());
            } elseif ($token instanceof Identifier || $token instanceof Delimiter) {
                self::assertSame($expectedToken[1], $token->getValue());
            } elseif ($token instanceof TNumber) {
                self::assertSame($expectedToken[1], $token->getSign());
                self::assertSame($expectedToken[2], $token->getRawValue());
            }
        }

        self::assertInstanceOf(EOF::class, $lexer->readNext());
    }

    public function anPlusBInputProvider(): array
    {
        return [
            [
                '4n',
                [
                    [Dimension::class, '', 4, 'n'],
                ],
            ],
            [
                '+4n',
                [
                    [Dimension::class, '+', 4, 'n'],
                ],
            ],
            [
                '4n-1',
                [
                    [Dimension::class, '', 4, 'n-1'],
                ],
            ],
            [
                '+4n-2',
                [
                    [Dimension::class, '+', 4, 'n-2'],
                ],
            ],
            [
                'n-1',
                [
                    [Identifier::class, 'n-1'],
                ],
            ],
            [
                'n+1',
                [
                    [Identifier::class, 'n'],
                    [IntegerNumber::class, '+', 1],
                ],
            ],
            [
                '+n+1',
                [
                    [Delimiter::class, '+'],
                    [Identifier::class, 'n'],
                    [IntegerNumber::class, '+', 1],
                ],
            ],
            [
                '-n-1',
                [
                    [Identifier::class, '-n-1'],
                ],
            ],
            [
                '4n + -4',
                [
                    [Dimension::class, '', 4, 'n'],
                    [Whitespace::class],
                    [Delimiter::class, '+'],
                    [Whitespace::class],
                    [IntegerNumber::class, '-', -4],
                ],
            ],
        ];
    }

    private function getLexer(string $input): Lexer
    {
        return new SpecificationCompliantLexer(new CompleteLexer(new SpecificationCompliantInputStream($input)));
    }
}
